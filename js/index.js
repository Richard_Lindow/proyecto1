	
$(function() {
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover']").popover();
	$('.carousel').carousel({
		interval: 4000
	})

	$('#masInfo').on('show.bs.modal', function (e){
		console.log('el modal se está mostrando');
		$('#masInfoBtn').removeClass('btn-outline-success');
		$('#masInfoBtn').addClass('btn-secondary');
		$('#masInfoBtn').prop('disabled', true);
	});
	$('#masInfo').on('shown.bs.modal', function (e){
		console.log('el modal se mostró');
	});
	$('#masInfo').on('hide.bs.modal', function (e){
		console.log('el modal se está oculando');
	});
	$('#masInfo').on('hidden.bs.modal', function (e){
		console.log('el modal se ocultó');
		$('#masInfoBtn').removeClass('btn-secondary');
		$('#masInfoBtn').addClass('btn-outline-success');
		$('#masInfoBtn').prop('disabled', false);
	});
});
